module Types.HelperTypes exposing (Password, Url, Uuid)


type alias Url =
    String


type alias Uuid =
    String


type alias Password =
    String
